const contentContainer = document.getElementById( 'content' )
const loadingSpinner = document.getElementById( 'loading-spinner' )
const dataSize = 20

const observer = new IntersectionObserver( entries => {
    const lastRow = entries[0]
    if ( !lastRow.isIntersecting ) return
    loadAndRenderData()
    observer.unobserve( lastRow.target )

}, { rootMargin: "50px" } )

const loadAndRenderData = () => {
    const xhttp = new XMLHttpRequest()
    loadingSpinner.classList.remove( 'loaded' )
    loadingSpinner.classList.add( 'loading' )
    xhttp.onerror = () => {
        sendMessage( "Something went wrong! please try again", "darkred" )
        loadingSpinner.classList.add( 'loaded' )
        loadingSpinner.classList.remove( 'loading' )
    }
    xhttp.onload = () => {
        JSON.parse( xhttp.responseText ).forEach( ( element, index ) => {
            const tr = document.createElement( 'tr' )
            const content = `<td scope="row">${element.id}</td>
                        <td>${element.first_name} ${element.last_name}</td>
                        <td>${element.email}</td>
                        <td>${element.address.state}, ${element.address.country}</td>
                        <td>${element.employment.title}</td>`
            tr.innerHTML = content
            contentContainer.appendChild( tr )
            if ( index === dataSize - 1 ) {
                observer.observe( tr )
            }
        } )
        sendMessage( `${dataSize} more loaded`, "darkgreen" )
        loadingSpinner.classList.add( 'loaded' )
        loadingSpinner.classList.remove( 'loading' )
    }
    xhttp.open( 'GET', `https://random-data-api.com/api/v2/users?size=${dataSize}` )
    xhttp.send()
}

const sendMessage = ( msg, color ) => {
    const ele = document.createElement( 'span' )
    ele.textContent = msg
    ele.classList.add( 'flash' )
    ele.style.color = color
    document.body.appendChild( ele )
    setTimeout( () => {
        document.body.removeChild( ele )
    }, 3000 )
}


console.log( "Lazy loading data" )
addEventListener( 'load', loadAndRenderData, false )