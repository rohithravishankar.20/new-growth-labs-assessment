const range = document.getElementById( 'range' )
const form = document.getElementById( 'form' )
const submitBtn = document.getElementById( 'submitBtn' )
const modalClose = document.getElementById( 'modalClose' )

const setHash = ( range ) => {
    if ( window.innerWidth < 670 )
        window.location.hash = `#${range}`
}
const sendMessage = ( msg, color ) => {
    const ele = document.createElement( 'span' )
    ele.textContent = msg
    ele.classList.add( 'flash' )
    ele.style.color = color
    document.body.appendChild( ele )
    setTimeout( () => {
        document.body.removeChild( ele )
    }, 3000 )
}

range.addEventListener( 'change', ( e ) => {
    let pricing = e.target.value <= 10 ? 1 : e.target.value <= 20 ? 2 : 3
    document.querySelector( '.active-box' ).classList.add( 'non-active-box' )
    document.querySelector( '.active-box' ).classList.remove( 'active-box' )
    document.getElementById( `pricing-${pricing}` ).classList.add( 'active-box' )
    document.getElementById( `pricing-${pricing}` ).classList.remove( 'non-active-box' )
    setHash( `pricing-${pricing}` )
} )

form.addEventListener( 'submit', ( e ) => {
    e.preventDefault()
    const name = e.target['name'].value
    const email = e.target['email'].value
    const orderComments = e.target['order-comments'].value
    e.target['name'].value = ''
    e.target['email'].value = ''
    e.target['order-comments'].value = ''

    if ( name && name.trim().length > 0 && email && email.trim().length > 0 && orderComments && orderComments.trim().length > 0 ) {
        const xhttp = new XMLHttpRequest()
        xhttp.onerror = ( e ) => {
            submitBtn.disabled = false
            submitBtn.innerHTML = "Sign up"
            modalClose.click()
        }
        xhttp.onload = () => {
            submitBtn.disabled = false
            submitBtn.innerHTML = "Sign up"
            modalClose.click()
        }
        xhttp.open( "POST", "https://forms.maakeetoo.com/formapi/454" )
        const formData = new FormData()
        formData.append( 'firstname', name )
        formData.append( 'email', email )
        formData.append( 'message', orderComments )
        submitBtn.disabled = true
        submitBtn.innerHTML = "Please wait..."
        xhttp.send( formData )
        console.clear()
    } else {
        sendMessage( "Fill all the fields", "darkred" )
    }


} )