# Next Growth Labs Assessment

##### Check live Demo [here](https://next-growth-labs-assessment.netlify.app/)

##### Screencast: https://drive.google.com/file/d/1-lvZJO9yAVwPJPzotWPvMzxKdCrLZCto/view?usp=sharing

## Section A : Making a simple website with vanilla JS

- **Task 1 - BootStrap**
- a. Use this [page](https://getbootstrap.com/docs/4.0/examples/pricing/), and create a modal - which gets called - whenever the user clicks on the pricing. The modal should contain a form - asking following details:
- [ ] Name
- [ ] Email
- [ ] Order Comments
- From the bootstrap template, one should be able to click on any of the buttons of the three pricing tables, and fill in a form.
- b. Use a _slider_ - which should allow user to scroll between the number of users - and suppose if the number of users are 0-10, then first plan should get highlighted, 10-20, the second plan should get highlighted, and so on.
- **Task 2 - vanilla JS**

`Sign up on https://forms.maakeetoo.com`

- from task 1 - the contents of form submission - should be populated there.
- The site **core web vitals report** should be taken using lighthouse in developer tool and screenshots recorded in the video .
- The candidate should try to optimise **Core web vitals** to meet the standard requirements for both **desktop** and **mobile**

### Implementation

- A slider is used to specify the number of users, which on input highlight the corresponding pricing card based on the users count.
  The form data is sent to https://forms.maakeetoo.com/ using XMLHttpRequest object.

## Section B: Lazy Loading To Avoid Pagination (vanilla js)

To avoid pagination, Frontend devs came up with the solution of lazy loading, so when a user reaches the end of the page, more results are automatically loaded to have a good user experience.
**Note**: **Please make sure you only use publicly available API**

- Avoid using any libraries.
- Generate any random data, after the user reaches the end of the current results.

### Implementation of Lazy loading

- API used: [Random Data API](https://random-data-api.com/)
- Technique: Using `IntersectionObserver`

## Deploying

Deployment using Netlify with automatic deployment support.
